
import * as VueRouter from 'vue-router'
import createDataBaseVue from './createDataBase.vue'
import selectDataBaseVue from './selectDataBase.vue'

const routes = [
    { path: '/create', component: createDataBaseVue },
    { path: '/select', component: selectDataBaseVue },
]

const router = VueRouter.createRouter({
    history: VueRouter.createWebHashHistory(),
    routes, 
})

export { router }
