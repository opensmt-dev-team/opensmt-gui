
import * as VueRouter from 'vue-router'
import createKeyVue from './createKey.vue'
import adminKeyVue from './adminKey.vue'

const routes = [
    { path: '/create', component: createKeyVue },
    { path: '/admin', component: adminKeyVue },
]

const router = VueRouter.createRouter({
    history: VueRouter.createWebHashHistory(),
    routes, 
})

export { router }
