import { createApp } from "vue";
import loaderViewVue from "./loaderView.vue";
import { router } from './loader'
import '@arco-design/web-vue/es/card/style/index'
import '@arco-design/web-vue/es/form/style/index'
import '@arco-design/web-vue/es/input/style/index'
import '@arco-design/web-vue/es/tooltip/style/index'
import '@arco-design/web-vue/es/button/style/index'
import '@arco-design/web-vue/es/checkbox/style/index'
import '@arco-design/web-vue/es/tabs/style/index'
import '@arco-design/web-vue/es/table/style/index'
const app = createApp(loaderViewVue);
app.use(router)
app.mount(`#home`)